# Script to expand a list of "Green Patent" codes, according to IPC and ENVTECH.

The full methodology and examples are published in:

Marinella Favot, Leyla Vesnic, Riccardo Priore, Andrea Bincoletto, Fabio Morea,
"Green patents and green codes: how different methodologies lead to different results",
Resources, Conservation & Recycling Advances,
2023,
200132,
ISSN 2667-3789,
https://doi.org/10.1016/j.rcradv.2023.200132.
https://www.sciencedirect.com/science/article/pii/S2667378923000044

## Abstract: 
Green patents are valid instrument to measure eco-innovation which aims at reducing the negative impact on the environment and the more efficient use of resources. There are three methodologies available to identify green patents based on the code classification: ENV-TECH (developed by OECD), IPC Green Inventory (WIPO) and Y02/Y04S Tagging scheme (EPO). Our results are the systematic organisation of green codes for each methodology and the algorithms for their periodic update; the application of these methodologies to two large datasets and the comparison of the three methodologies. The relevant findings are that ENV-TECH and IPC Green Inventory should be used in combination to identify more green patents, with the inclusion of the CPC ENV-TECH green codes when applicable. The Tagging scheme identifies fewer green patents than the combination of the other two methodologies. The three methodologies overlap only partially (in 22.47% of cases) and their mutual integration is recommended.
Keywords: Eco-innovation; Green patent; Green code; ENV-TECH; IPC Green Inventory; Y02/Y04 Tagging scheme

 
