# Author: Andrea Bincoletto @ Area Science Park
# Package: GreenCodes version 1.0
# Description: python program to expand a list of "Green Patent" based on ENV TECH classification, 
#               Methodology and examples are available in 
#               "Green patents and green codes: how different methodologies lead to different results" 
#               Marinella Favot, Leyla Vesnic, Riccardo Priore, Andrea Bincoletto, Fabio Morea
#               Area Science Park, Padriciano 99 34149, Trieste, Italy

# SPDX-License-Identifier: CC-BY-4.0

# load libraries
import pandas as pd
import os

# list of folder containing csv files
lista_dir = ['1_CPC','2_CPC','3_CPC','4_CPC','5_CPC','6_CPC','7_CPC','8_CPC','1_IPC']

# function to create a list of the directory
def lista_directory(path):
    lista_dd = []
    for x in lista_dir:
        lista_dd.append(path + x + "\\")
    print(f'directory data: {lista_dd}')
    return(lista_dd)

subdirs_to_process = lista_directory(".\\data\\ENVTECH\\")

# function to open every csv file in every folders
def lista_file_csv(dd):
    lista_file = []
    for root, dirs, files in os.walk(dd):
        for name in files:
            if 'csv' in name:
                lista_file.append(os.path.join(root, name))
    n = len(lista_file)
    print(f'The directory contains {n} files')
    return (lista_file)

# function to read the csv files in the folder
def carica_dati_da_csv(ff):
    print(f'Elaboration of the file: {ff} \n ')
    df = pd.read_csv(ff, sep = ',', encoding='utf-8-sig', dtype=str)
    df['Code'] = df['Code'].str.replace(' ','')
    return(df)

# append of all the codes
results = pd.DataFrame()
for dd in subdirs_to_process:
    print(f'>>> Start elaboration {dd}')
    file_da_elaborare = lista_file_csv(dd)
    for ff in file_da_elaborare:   
        df = carica_dati_da_csv(ff)
        results = results.append(df)
        results = results.drop_duplicates()
        print(results)
        print("---------------------")

print("End of the process")

# save .csv of results
results.to_csv(".\\results\\ENVTECH_green_codes.csv", sep='|',encoding='utf-8-sig', index=False)
print('Script completed')


